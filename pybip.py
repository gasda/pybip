#!/usr/bin/env python
''' Python Script to build a BIP on the fly
    Dylan Reinhold 04/05/2016
    See LICENSE.txt for license info
'''
from __future__ import print_function
import os
import sys
import errno
import shutil
import datetime
import glob
from xml.etree import ElementTree
#from ElementTree_pretty import prettify

import zipfile

VERSION='0.0.1'

class BIP(object):
    update_levels = { 'SILENT' : 0,
                      'DATA' : 1,
                      'PROG' : 2,
                      'RETAIL' : 3, }
           
    def __init__(self,rcf_filename,zip_filename=None):
        self.rcf_filename = rcf_filename
        self.tmp_dir = 'tbuild'
        self.local = True
        self.update_level = 'SILENT'
        self.package_file_xmls = []
        
        
        document = ElementTree.parse( self.rcf_filename )
        file_nodes = document.find( 'rcf' )
        self.filenames = [node.text for node in file_nodes.getchildren()]

    def set_update_level(self,new_level):
        if self.update_level == 'RETAIL':
            return #nothing to do we are already at the higest level
        if self.update_levels[new_level] > self.update_levels[self.update_level]:
            self.update_level = new_level
        return

    def get_files_rev(self):
        ''' Return files in reverse order '''
        return iter(reversed(self.filenames))

    def get_files(self):
        return iter(self.filenames)

    @staticmethod
    def indent(elem, level=0):
        i = "\n" + level*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                BIP.indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def build_bip_pck(self):
        bizerba = ElementTree.Element("bizerba")
        comment = ElementTree.Comment('Built using pybip version {}'.format(VERSION))
        bizerba.append(comment)
        ElementTree.SubElement(bizerba, 'packagetype').text = 'BIP'
        curr_dt = datetime.datetime.now()
        ElementTree.SubElement(bizerba, 'date').text = curr_dt.strftime('%Y-%m-%d')
        ElementTree.SubElement(bizerba, 'time').text = curr_dt.strftime('%H:%M:%S')
        ElementTree.SubElement(bizerba, 'type').text = self.update_level
        self.indent(bizerba)
        tree = ElementTree.ElementTree(bizerba)
        tree.write("pck__inf.xml", xml_declaration=True, encoding='UTF-8')

    @staticmethod
    def get_new_pck_filename(z_filename):
        return 'pck__infs/' + z_filename[0:-4] + '_pck__inf.xml' 

    def get_new_zip_filename(self):
        return self.rcf_filename[0:-4] + '.zip'

    def get_install_type(self, z_filename):
        pck_name = self.get_new_pck_filename(z_filename)
        document = ElementTree.parse( pck_name )
        types = document.find( 'type' )
        return types.text

    def get_pck_info(self, z_filename):
        pck_name = self.get_new_pck_filename(z_filename)
        document = ElementTree.parse( pck_name )
        attribs = ( 'matno', 'build', 'version', 'stoff' )
        p_data = {}
        for a in attribs:
            try:
                p_data[a] = document.find( a ).text
            except:
                p_data[a] = ''
            if p_data[a] is None:
                p_data[a] = ''
        p_data['filename'] = z_filename
        self.package_file_xmls.append(p_data)
        
    def get_zip_pck(self, z_filename):
        z_file = zipfile.ZipFile(z_filename, "r")
        z_file.extract('pck__inf.xml')
        z_file.close()
        new_name = self.get_new_pck_filename(z_filename)
        shutil.move('pck__inf.xml', new_name)

    def build_bip_sequence_file(self):
        filename = 'sequence.xml'
        bizerba = ElementTree.Element("bizerba")
        comment = ElementTree.Comment('Built using pybip version {}'.format(VERSION))
        bizerba.append(comment)
        curr_dt = datetime.datetime.now()
        ElementTree.SubElement(bizerba, 'date').text = curr_dt.strftime('%Y-%m-%d')
        ElementTree.SubElement(bizerba, 'checksum').text = '-'
        ElementTree.SubElement(bizerba, 'sequence').text = ' '
        self.indent(bizerba)
        tree = ElementTree.ElementTree(bizerba)
        tree.write(filename, xml_declaration=True, encoding='UTF-8')

    def build_bip_sequence_customer_file(self):
        filename = 'sequence_customer.xml'
        bizerba = ElementTree.Element("bizerba")
        comment = ElementTree.Comment('Built using pybip version {}'.format(VERSION))
        bizerba.append(comment)
        curr_dt = datetime.datetime.now()
        ElementTree.SubElement(bizerba, 'date').text = curr_dt.strftime('%Y-%m-%d')
        ElementTree.SubElement(bizerba, 'checksum').text = '-'
        seq = ElementTree.SubElement(bizerba, 'sequence')
        for xml in reversed(self.package_file_xmls):
            attr_dict = dict(xml)
            del attr_dict['filename']
            ElementTree.SubElement(seq, 'filename', attrib=attr_dict ).text = xml['filename']

        self.indent(bizerba)
        tree = ElementTree.ElementTree(bizerba)
        tree.write(filename, xml_declaration=True, encoding='UTF-8')


    def build_main_bip_zip(self):
        filename = self.get_new_zip_filename()
        z_file = zipfile.ZipFile(filename, "w")
        last_zip = self.package_file_xmls[-1]['filename']
        zip_loc = "bizerba/update/loc/{}".format(last_zip)
        z_file.write(last_zip, zip_loc)
        local_files = ('pck__inf.xml', 'sequence.xml', 'sequence_customer.xml')
        for lf in local_files:
            z_file.write(lf)
        
        for f in glob.glob('pck__infs/*.xml'):
            z_file.write(f)
       
        z_file.close()
        shutil.move(filename, os.path.join('../', filename))
        os.chdir('../')
        shutil.rmtree(self.tmp_dir)
        return filename

    def build(self):
        prev_zip = None
        try:
            shutil.rmtree(self.tmp_dir)
        except OSError as exception:
            if exception.errno != errno.ENOENT:
                raise
        try:
            os.makedirs(self.tmp_dir)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise
        try:
            os.makedirs(os.path.join(self.tmp_dir, 'pck__infs'))
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise
        curr_dir = os.getcwd()
        os.chdir(self.tmp_dir)
        
        
        for z in self.get_files_rev():
            z_filename = os.path.basename(z)
            if self.local:
                z = os.path.join(curr_dir,z_filename)
            try:
                os.remove(z_filename)
            except:
                pass
            shutil.copyfile(z, z_filename)
            self.get_zip_pck(z_filename)
            update_type = self.get_install_type(z_filename)
            self.set_update_level(update_type)
            self.get_pck_info(z_filename)
            if prev_zip is not None:
                z_file = zipfile.ZipFile(z_filename, "a")
                zip_loc = "bizerba/update/loc/{}".format(prev_zip)
                z_file.write(prev_zip, zip_loc)
                z_file.close()
                os.remove(prev_zip)
            prev_zip = z_filename
         
        self.build_bip_pck()
        self.build_bip_sequence_file()
        self.build_bip_sequence_customer_file()
        return self.build_main_bip_zip()

if __name__ == '__main__':

    print("Starting pybip builder version",VERSION)
    # Open the rcf file
    try:
        rcf_file = sys.argv[1]
    except IndexError:
        print("Error: Missing rcf file as argument")
        sys.exit(15)
    print("Using file {} as input".format(rcf_file))
    bip = BIP(rcf_file)
    # Get list of zip files to build the package
    #for f in bip.get_files():
    #    print(f)
    # Combin them by adding the next zip into 
    new_zip = bip.build()
    print("Built new BIP as", new_zip)
